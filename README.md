# Go Exercism Challenges

This is a collection of my solutions to the Go challenges on [Exercism](https://exercism.org/tracks/go).
